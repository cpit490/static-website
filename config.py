import multiprocessing

bind = "127.0.0.1:8080"
workers = multiprocessing.cpu_count() * 2 + 1
print("Running Gunicorn, the WSGI HTTP server, with {} workers".format(workers))
print("Web Server listening at http://{0}".format(bind))
print("Press Ctrl+C to quit")