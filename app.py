from flask import Flask, render_template
import os
import socket
import datetime
import multiprocessing

app = Flask(__name__)
hostname = socket.gethostname()
ts = datetime.datetime.now()
_workers = multiprocessing.cpu_count() * 2 + 1

@app.route('/')
def home():
    return render_template('index.html', hostname=hostname, ts=ts)

@app.route('/health')
def health():
    return {"status": "ok"}, 200

# read host and port from the environment
HOST = os.getenv('HOST', '127.0.0.1')
PORT = int(os.getenv('PORT', 4000))

if __name__ == '__main__':
    app.run(debug=False, port=PORT, host=HOST)
