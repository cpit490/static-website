# Static Website Sample with WSGI GUnicorn

This is a Python Flask application with Gunicorn. Flask is a popular web framework for building web applications in Python. It comes with a local web server that is used for development purposes only while developing and testing the application. In production, a WSGI (Web Server Gateway Interface) server like Gunicorn is needed to provide a robust, scalable, and secure web server environment. This Flask application is a simple web server that serves a static HTML template and is used in the [load balancer lab activity](https://cpit490.gitlab.io/labs/lab-6/).


## Usage

We will create a virtual environment, activate it, install dependencies, and starts the application using gunicorn.

The server will start at `http://127.0.0.1:8080` with a number of worker processes equal to `(2 x num_of_cpu_cores) + 1`. You may edit the `config.py` file if you wish to change the default host, port, or worker processes.


```shell
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
gunicorn -c config.py app:app
```

- When you're done and the server is no longer needed, deactivate the virtual environment using the command `deactivate`.

### Explanation
- `python -m venv .venv`
  - We create a virtual environment in a directory called `.venv`. A virtual environment is a self-contained Python environment that you can use to install packages without affecting the system-wide Python installation.
- `source .venv/bin/activate`
  - This command activates the virtual environment created earlier.
- `pip install -r requirements.txt`
  - We install the dependencies listed in the _requirements.txt_ file.
- `gunicorn -c config.py app:app`.
  - This command starts the application using Gunicorn, a WSGI HTTP server for Python web applications. 
    - `app:app` tells Gunicorn to load the `app` module in a file named `app.py`. 
    - The `config.py` file contains the following configuration:
      - It allocates a number worker processes equals to `(2 x num_of_cpu_cores) + 1` to handle multiple requests at the same time. 
      - It sets the binding address to `127.0.0.1` and port `8080`, making the server accessible on loopback interface of the host machine.

> **Note:** Even though the Flask app is configured to run on localhost and port 4000, this configuration is ignored when we run the Flask app with Gunicorn. Thus, when we run the Flask app with Gunicorn, the host and port that Gunicorn binds to will be used instead.

## License
MIT